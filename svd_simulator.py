from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import ipywidgets as wd
from IPython.display import display


def load(path, h=None, w=None):
    """
    Load and resize an image.

    Parameters:
    - path: Path to the image file.
    - h: Desired height.
    - w: Desired width.
    """
    im = Image.open(path)
    if h is None:
        h = im.height
    if w is None:
        w = im.width * h / im.height
    return im.resize((int(w), int(h)))


def show(img):
    """Display the image."""
    im = Image.fromarray((img.clip(0, 1) * 255.).astype(np.uint8))
    display(im)


def show_old(img, figsize=(12, 12)):
    """Display the image."""
    fig, ax = plt.subplots(figsize=figsize)
    ax.imshow(img.clip(0, 1))
    plt.axis('off')
    plt.show()
    plt.close(fig)


class SVDSimulator:
    def __init__(self, gallery, height=200, max_no_components=100):
        """
        Initialize the SVD Simulator.

        Parameters:
        - gallery: List of image paths.
        - height: Display height for images.
        - max_no_components: Maximum number of singular values to use.
        """
        self.gallery = gallery
        self.height = height
        self.max_no_components = max_no_components
        self.no_components = 10  # Initial number of singular values
        self.image = None
        self.USVH_RGB = None
        self.items = []
        self.sld = wd.IntSlider(value=self.no_components, min=0, max=self.max_no_components, step=1,
                                description='components:', continuous_update=False)
        self.out = wd.Output()
        self.prepare_gallery()
        self.sld.observe(self.update_sld_show, names='value')
        self.select_image(0)
        self.update_composition()

    def prepare_gallery(self):
        """Prepare the image gallery with thumbnails and selection buttons."""
        for i, image_path in enumerate(self.gallery):
            thumb = load(image_path, 128)
            thumbnail = wd.Output()
            with thumbnail:
                display(thumb)
            button = wd.Button(description="Select Me!")
            button.on_click(self.make_on_click(i))
            self.items.append(wd.VBox([thumbnail, button]))

    def make_on_click(self, index):
        """Create an on-click event for image selection."""
        def on_click(b):
            self.select_image(index)
            self.update_composition()
        return on_click

    def select_image(self, index):
        """Select an image from the gallery and compute its SVD."""
        self.image = np.array(load(self.gallery[index], self.height)) / 255.0
        self.USVH_RGB = [np.linalg.svd(self.image[:, :, i], full_matrices=False) for i in range(3)]

    def compose(self, n):
        """
        Reconstruct the image using the first n singular values.

        Parameters:
        - n: Number of singular values.
        """
        def compose_channel(u, s, vh):
            return u[:, :n] @ np.diag(s[:n]) @ vh[:n, :]

        rgb = [compose_channel(*c) for c in self.USVH_RGB]
        return np.stack(rgb, axis=2)

    def update_composition(self):
        """Update the displayed image based on the current number of singular values."""
        with self.out:
            self.out.clear_output(True)
            img_ = self.compose(self.no_components)
            show(img_)

    def update_sld_show(self, change):
        """Handle slider value changes."""
        self.no_components = change['new']
        self.update_composition()

    def display(self):
        """Display the widgets and output."""
        display(self.sld, self.out)
        display(wd.HBox(self.items))
