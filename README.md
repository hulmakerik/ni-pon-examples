# ni-pon-examples

Welcome to the **ni-pon-examples** repository! This collection of educational materials is designed to help you explore fundamental concepts in linear algebra through interactive Jupyter notebooks. Dive into step-by-step simulations of the QR algorithm and discover the intricacies of image compression using Singular Value Decomposition (SVD).

## Table of Contents

- [Overview](#overview)
- [QR Algorithm Simulator](#qr-algorithm-simulator)
  - [Understanding the QR Algorithm](#understanding-the-qr-algorithm)
  - [Interactive Notebook on Google Colab](#interactive-notebook-on-google-colab)
- [SVD Image Compression](#svd-image-compression)
  - [Understanding SVD](#understanding-svd)
  - [Interactive Notebook on Google Colab](#interactive-notebook-on-google-colab-1)
- [Getting Started](#getting-started)
- [Requirements](#requirements)
- [Contributing](#contributing)
- [Contact](#contact)

---

## Overview

This repository is tailored for students, educators, and enthusiasts keen on visualizing and interacting with linear algebra concepts.

## QR Algorithm Simulator

### Understanding the QR Algorithm

The [QR algorithm](https://en.wikipedia.org/wiki/QR_algorithm) is a method in numerical linear algebra for calculating the eigenvalues and eigenvectors of a matrix. By decomposing a matrix into an orthogonal matrix **Q** and an upper triangular matrix **R**, and iteratively applying this decomposition, the algorithm converges to a form where the eigenvalues become apparent.

### Interactive Notebook on Google Colab

Experience the QR algorithm in action with our interactive notebook:

- **[QR Algorithm Simulator on Google Colab](https://colab.research.google.com/drive/1LLDK7Zd4uMh7wty_Mfiij3ZLBTgJP5-D?usp=sharing)**

This notebook allows you to:

- Step through each iteration of the algorithm.
- Visualize how the matrix transforms over time.
- Modify input matrices to see different outcomes.
- Compare the [Householder Reflection](https://en.wikipedia.org/wiki/Householder_transformation), and [Givens Rotation](https://en.wikipedia.org/wiki/Givens_rotation)

## SVD Image Compression

### Understanding SVD

[Singular Value Decomposition (SVD)](https://en.wikipedia.org/wiki/Singular_value_decomposition) is a technique that factors a matrix into three components: an initial matrix of left singular vectors (**U**), a diagonal matrix of singular values (**Σ**), and a matrix of right singular vectors (**V**ᵗ). In the context of image processing, SVD can be used to compress images by retaining only the most significant singular values, reducing file size while preserving essential features.

### Interactive Notebook on Google Colab

Discover the effects of image compression with our SVD notebook:

- **[SVD Image Compression on Google Colab](https://colab.research.google.com/drive/1XXHy6BmFcsgRtggmx70-KhMRCoMJ4Nz4?usp=sharing)**

In this notebook, you can:

- Decompose images into lower-dimensional matrices.
- Reconstruct images using a subset of singular values.
- Observe how compression affects image quality and introduces artifacts.

## Getting Started

To explore the notebooks locally:

1. **Clone the repository:**

   ```bash
   git clone git@gitlab.com:hulmaker/ni-pon-examples.git
   ```

2. **Navigate to the directory:**

   ```bash
   cd ni-pon-examples
   ```

3. **Install the required packages:**

   ```bash
   pip install -r requirements.txt
   ```

4. **Launch Jupyter Notebook or JupyterLab:**

   ```bash
   jupyter notebook
   ```

   or

   ```bash
   jupyter lab
   ```

5. **Open the notebooks:**

   - `QR_simulation.ipynb` for the QR algorithm simulator.
   - `image_SVD.ipynb` for the SVD image compression demo.

## Requirements

- Python 3.x
- Jupyter Notebook or JupyterLab
- Required Python packages listed in `requirements.txt`

## Contributing

We appreciate contributions to enhance these educational materials:

- **Reporting Issues:** If you encounter any problems or have suggestions, please [open an issue](https://github.com/yourusername/ni-pon-examples/issues).
- **Pull Requests:** Feel free to fix bugs or add new features by submitting a pull request.

## Contact

For questions or feedback, please reach out to:

- **Erik** - [erik@hulmak.cz](mailto:erik@hulmak.cz)

---

Happy learning and exploring!
