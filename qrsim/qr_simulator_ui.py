from .qr_simulator import QRSimulator
import ipywidgets as wd
from IPython.display import display, Markdown, Latex


class QRSimulatorUI:
    def __init__(self, A, **simulator_colors):
        self.method = 'householder'
        self.sim = QRSimulator(A, **simulator_colors)
        self.i = 0
        self.out = wd.Output()
        self.data_out1 = wd.Output()
        self.data_out2 = wd.Output()
        # Controls
        self.btns = [
            wd.Button(description="next"),
            wd.Button(description="previous"),
            wd.Button(description="reset-Householder"),
            wd.Button(description="reset-Givens"),
        ]
        self.btns[0].on_click(self.next_i)
        self.btns[1].on_click(self.previous_i)
        self.btns[2].on_click(self.reset_householder)
        self.btns[3].on_click(self.reset_givens)
        self.graphical_output = wd.HBox([self.out, wd.VBox(self.btns)])
        self.data_output = wd.HBox([self.data_out1, self.data_out2])
        self.sim.run_qr_decomposition(self.method)
        self.update_output()

    def update_output(self):
        with self.out:
            self.out.clear_output(True)
            self.sim.plot_step(self.i, self.method)
        with self.data_out1:
            self.data_out1.clear_output(True)
            if self.method == 'givens':
                display(Markdown("## $Q_i^T$"))
                print(str(self.sim.qis[self.i].T))
            else:
                display(Markdown("## $Q_i$"))
                print(str(self.sim.qis[self.i]))
        with self.data_out2:
            self.data_out2.clear_output(True)
            display(Markdown("## $R_i$"))
            print(str(self.sim.rs[self.i]))

    def next_i(self, b=None):
        self.i = min(self.i+1, len(self.sim.ps))
        self.update_output()

    def previous_i(self, b=None):
        self.i = max(self.i-1, 0)
        self.update_output()

    def reset(self, next_method):
        self.method = next_method
        self.sim.run_qr_decomposition(next_method)
        self.i = 0
        self.update_output()

    def reset_householder(self, b=None):
        self.reset('householder')

    def reset_givens(self, b=None):
        self.reset('givens')

    def display(self):
        display(self.graphical_output)
